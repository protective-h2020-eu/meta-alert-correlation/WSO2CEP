/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * WSO2 Inc. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wso2.extension.siddhi.streamprocessor.MetaAlertAggregatorStreamProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.wso2.siddhi.core.config.ExecutionPlanContext;
import org.wso2.siddhi.core.event.ComplexEventChunk;
import org.wso2.siddhi.core.event.stream.StreamEvent;
import org.wso2.siddhi.core.event.stream.StreamEventCloner;
import org.wso2.siddhi.core.event.stream.populater.ComplexEventPopulater;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.VariableExpressionExecutor;
import org.wso2.siddhi.core.query.processor.Processor;
import org.wso2.siddhi.core.query.processor.stream.StreamProcessor;
import org.wso2.siddhi.query.api.definition.AbstractDefinition;
import org.wso2.siddhi.query.api.definition.Attribute;



/**
 * Event_Aggregator.
 */
public class MetaAlert_AggregatorStreamProcessor extends StreamProcessor {
	
	private enum Fields{
		Aggregated_UID,
		Aggregated_DetectTime,
		Aggregated_Category,
		Aggregated_AggrID,
		Aggregated_TopAssetCriticality,
		Aggregated_Rule,
		Aggregated_MAQuality,
		Aggregated_Source_Asset,
		Aggregated_Source_MaxAssetCriticality,
		Aggregated_Source_IP,
		Aggregated_Source_Proto,
		Aggregated_Source_Port,
		Aggregated_Source_hostname,
		Aggregated_Target_Asset,
		Aggregated_Target_MaxAssetCriticality,
		Aggregated_Target_IP,
		Aggregated_Target_Proto,
		Aggregated_Target_Port,
		Aggregated_Target_hostname,
		Aggregated_Email;
		
		public static String[] getNames(Class<? extends Enum<?>> e) {
		    return Arrays.toString(e.getEnumConstants()).replaceAll("^.|.$", "").split(", ");
		}
	};
		
	private int paramCount = 0;
    private VariableExpressionExecutor Target, Source,Id;  
        	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] currentState() {
		return new Object[0];
	}

	@Override
	public void restoreState(Object[] arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected List<Attribute> init(AbstractDefinition arg0, ExpressionExecutor[] arg1, ExecutionPlanContext arg2) {

		paramCount = attributeExpressionLength;
		Source = (VariableExpressionExecutor) attributeExpressionExecutors[Fields.Aggregated_Source_IP.ordinal()];
		Target = (VariableExpressionExecutor) attributeExpressionExecutors[Fields.Aggregated_Target_IP.ordinal()];
		Id = (VariableExpressionExecutor) attributeExpressionExecutors[Fields.Aggregated_UID.ordinal()];
		
		String[] Meta_Alert_Attributes = Fields.getNames(Fields.class);
		
		ArrayList<Attribute> attributes = new ArrayList<Attribute>(paramCount);
		for (int itr = 0; itr < Meta_Alert_Attributes.length; ++itr) {
	        attributes.add(new Attribute(Meta_Alert_Attributes[itr],Attribute.Type.STRING));
	    }
	    return attributes;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void process(ComplexEventChunk<StreamEvent> streamEventChunk, Processor nextProcessor, StreamEventCloner streamEventCloner,
		ComplexEventPopulater complexEventPopulater) {
		StreamEvent streamEvent = null;
		ComplexEventChunk<StreamEvent> returnEventChunk = new ComplexEventChunk<StreamEvent>();		
		HashMap<String,String[]> events = new HashMap<String, String[]>();
        while (streamEventChunk.hasNext()) {
        	streamEvent = streamEventChunk.next();
        	String key=getKeyToAggregate(events,Fields.Aggregated_AggrID.ordinal(),(String)streamEvent.getBeforeWindowData()[Fields.Aggregated_AggrID.ordinal()],(String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Rule.ordinal()] );
    	    //boolean ContainKey = events.containsKey((String)Id.execute(streamEvent));

            if (key!=null) {
            	String[] aux = events.get(key);
            	for (int i = 0; i < aux.length; i++) {
            		if (i == Fields.Aggregated_AggrID.ordinal() || i == Fields.Aggregated_Category.ordinal() || i == Fields.Aggregated_Email.ordinal() ||
            				i == Fields.Aggregated_Source_IP.ordinal() || i == Fields.Aggregated_Source_Proto.ordinal() || i == Fields.Aggregated_Source_Port.ordinal() ||
            				i == Fields.Aggregated_Target_IP.ordinal() || i == Fields.Aggregated_Target_Proto.ordinal() || i == Fields.Aggregated_Target_Port.ordinal() ||
            				i == Fields.Aggregated_Rule.ordinal() || i == Fields.Aggregated_Source_hostname.ordinal() || i == Fields.Aggregated_Target_hostname.ordinal()) {
            			String current_attribute = (String)streamEvent.getBeforeWindowData()[i];
            			if (current_attribute!= null) {
            				Set<String> subElements = new HashSet<String>(Arrays.asList(aux[i].split(",")));
            				subElements.addAll(Arrays.asList(current_attribute.split(",")));
            				subElements.remove("undefined");
            				subElements.remove("");
            				subElements.remove(null);
            				aux[i]=StringUtils.join(subElements,",");

            			}
            		}else if (i == Fields.Aggregated_MAQuality.ordinal() || i == Fields.Aggregated_TopAssetCriticality.ordinal() || i == Fields.Aggregated_Source_MaxAssetCriticality.ordinal()
            				 || i == Fields.Aggregated_Target_MaxAssetCriticality.ordinal()) {
            			if (!String.valueOf(streamEvent.getBeforeWindowData()[i]).trim().isEmpty() && !aux[i].trim().isEmpty() && Float.parseFloat(String.valueOf(streamEvent.getBeforeWindowData()[i])) > Float.parseFloat(aux[i])) {
            				aux[i] = String.valueOf(streamEvent.getBeforeWindowData()[i]);            				
            			}
            		}
            	}
            	events.put(key,aux);
            }else {
            	String newID=UUID.randomUUID().toString();
            	events.put(newID, new String[] {
            				newID,
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_DetectTime.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Category.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_AggrID.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_TopAssetCriticality.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Rule.ordinal()], 
            			    String.valueOf(streamEvent.getBeforeWindowData()[Fields.Aggregated_MAQuality.ordinal()]),
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Source_Asset.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Source_MaxAssetCriticality.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Source_IP.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Source_Proto.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Source_Port.ordinal()], 
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Source_hostname.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Target_Asset.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Target_MaxAssetCriticality.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Target_IP.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Target_Proto.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Target_Port.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Target_hostname.ordinal()],
            			   (String)streamEvent.getBeforeWindowData()[Fields.Aggregated_Email.ordinal()]});
            }
        }
        Set<String> events_targets = events.keySet();
        for (String s : events_targets) {
        	 	StreamEvent clonedEvent = streamEventCloner.copyStreamEvent(streamEvent);
	        	complexEventPopulater.populateComplexEvent(clonedEvent, events.get(s));
	        	returnEventChunk.add(clonedEvent);
        }
        nextProcessor.process(returnEventChunk);
        streamEventChunk.clear();

	}
	
	/**
	 * @param events events to compare
	 * @param field field to compare
	 * @param value value to be compared on the events
	 * @return return the key in the events that match with the value/field
	 */
	protected String getKeyToAggregate(HashMap<String, String[]> events, int field, String value, String rule){
		String key =null;
		Set<String> valueList=new HashSet<String>(Arrays.asList(value.split(",")));
		Set<String> key_events = events.keySet();
	    for (String k : key_events) {
	    	Set<String> fieldList= new HashSet<String>(Arrays.asList(events.get(k)[field].split(",")));
	    	fieldList.retainAll(valueList);
	    	if(fieldList.size()>0 && events.get(k)[Fields.Aggregated_Rule.ordinal()].equals(rule)) {
	    		return k;
	    	}
	    }
		return key;
	}
}
