package org.wso2.extension.siddhi.streamprocessor.EventAggregatorStreamProcessor;

public enum EventFields {

	Aggregated_DETECTTIME("Aggregated_Detecttime"),
	Aggregated_AGGRID("Aggregated_Aggrid"),
	Aggregated_SOURCE_IP("Aggregated_Source_Ip"),
	Aggregated_SOURCE_PROTO("Aggregated_Source_Proto"),
	Aggregated_SOURCE_PORT("Aggregated_Source_Port"),
	Aggregated_SOURCE_HOSTNAME("Aggregated_Source_Hostname"),
	Aggregated_TARGET_IP("Aggregated_Target_Ip"),
	Aggregated_TARGET_PROTO("Aggregated_Target_Proto"),
	Aggregated_TARGET_PORT("Aggregated_Target_Port"),
	Aggregated_TARGET_HOSTNAME("Aggregated_Target_Hostname"),
	Aggregated_CATEGORY("Aggregated_Category"),
	Aggregated_MAQUALITY("Aggregated_Maquality"),
	Aggregated_EMAIL("Aggregated_Email"),
	Aggregated_UID("Aggregated_Uid"),
	Aggregated_RULE("Aggregated_Rule");
     
    private String state;
     
    private EventFields(final String state){
        this.state = state;
    }
     
    public String getEventFields(){
        return this.state;
    }
 
    @Override
    public String toString(){
        return this.state;
    }
 
    public String getName(){
    	
        return this.name();
    }
}
