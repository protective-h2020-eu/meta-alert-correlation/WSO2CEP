/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * WSO2 Inc. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wso2.extension.siddhi.streamprocessor.EventAggregatorStreamProcessor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.wso2.siddhi.core.config.ExecutionPlanContext;
import org.wso2.siddhi.core.event.ComplexEventChunk;
import org.wso2.siddhi.core.event.stream.StreamEvent;
import org.wso2.siddhi.core.event.stream.StreamEventCloner;
import org.wso2.siddhi.core.event.stream.populater.ComplexEventPopulater;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.VariableExpressionExecutor;
import org.wso2.siddhi.core.query.processor.Processor;
import org.wso2.siddhi.core.query.processor.stream.StreamProcessor;
import org.wso2.siddhi.query.api.definition.AbstractDefinition;
import org.wso2.siddhi.query.api.definition.Attribute;

/**
 * Event_Aggregator.
 */
public class Event_AggregatorStreamProcessor extends StreamProcessor {
	
	private int paramCount = 0;
    private VariableExpressionExecutor Target, Source,Email; 
        	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] currentState() {
		return new Object[0];
	}

	@Override
	public void restoreState(Object[] arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected List<Attribute> init(AbstractDefinition arg0, ExpressionExecutor[] arg1, ExecutionPlanContext arg2) {
		paramCount = attributeExpressionLength;
		Target = (VariableExpressionExecutor) attributeExpressionExecutors[EventFields.Aggregated_TARGET_IP.ordinal()];
		Source = (VariableExpressionExecutor) attributeExpressionExecutors[EventFields.Aggregated_SOURCE_IP.ordinal()];
		Email = (VariableExpressionExecutor) attributeExpressionExecutors[EventFields.Aggregated_EMAIL.ordinal()];

		ArrayList<Attribute> attributes = new ArrayList<Attribute>(paramCount);
		for (EventFields e:EventFields.values()) {
	        attributes.add(new Attribute(e.name(),Attribute.Type.STRING));
	    }
	    return attributes;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void process(ComplexEventChunk<StreamEvent> streamEventChunk, Processor nextProcessor, StreamEventCloner streamEventCloner,
		ComplexEventPopulater complexEventPopulater) {
		
		StreamEvent streamEvent = null;
		ComplexEventChunk<StreamEvent> returnEventChunk = new ComplexEventChunk<StreamEvent>();		
		HashMap<String,String[]> events = new HashMap<String, String[]>();
        while (streamEventChunk.hasNext()) {
        	streamEvent = streamEventChunk.next();
        	String rule="TimeWindow_Events";
        	String key = getEmailDomain((String) Email.execute(streamEvent));
        	if(key==null) {
        		key=(String) Source.execute(streamEvent)+"t";
        	}else {
        		rule="Same_EmailDomain";
        	}
     	    boolean ContainKey = events.containsKey(key);
            if (ContainKey) {
            	String[] aux = events.get(key);
            	for (int i = 0; i < aux.length; i++) {
            		if (i!= EventFields.Aggregated_DETECTTIME.ordinal() &&
            				i != EventFields.Aggregated_TARGET_IP.ordinal() &&
            				i != EventFields.Aggregated_MAQUALITY.ordinal() &&
            				i != EventFields.Aggregated_UID.ordinal() &&
            				i != EventFields.Aggregated_RULE.ordinal()) {    			
            			String current_attribute = (String)streamEvent.getBeforeWindowData()[i];
            			if (current_attribute!= null) {
            				Set<String> subElements = new HashSet<String>(Arrays.asList(aux[i].split(",")));
            				subElements.addAll(Arrays.asList(current_attribute.split(",")));
            				subElements.remove("undefined");
            				subElements.remove("");
            				subElements.remove(null);
            				aux[i]=StringUtils.join(subElements,",");

            			}
            		}else if (i == EventFields.Aggregated_MAQUALITY.ordinal()) {
            			if (Float.parseFloat(String.valueOf(streamEvent.getBeforeWindowData()[i])) > Float.parseFloat(aux[i])) {
            				aux[i] = String.valueOf(streamEvent.getBeforeWindowData()[i]);            				
            			}
            		}
            	}
            	events.put(key,aux);
            	
            }else {
             	events.put(key, new String[] 
             			{(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_DETECTTIME.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_AGGRID.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_SOURCE_IP.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_SOURCE_PROTO.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_SOURCE_PORT.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_SOURCE_HOSTNAME.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_TARGET_IP.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_TARGET_PROTO.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_TARGET_PORT.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_TARGET_HOSTNAME.ordinal()],
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_CATEGORY.ordinal()],
             					String.valueOf(streamEvent.getBeforeWindowData()[EventFields.Aggregated_MAQUALITY.ordinal()]),
             					(String)streamEvent.getBeforeWindowData()[EventFields.Aggregated_EMAIL.ordinal()],
             					UUID.randomUUID().toString(),rule});
            }
        }
       
        
        events= processSingleEvents(events);
        
        Set<String> events_targets = events.keySet();
        for (String s : events_targets) {
        	 	StreamEvent clonedEvent = streamEventCloner.copyStreamEvent(streamEvent);
	        	complexEventPopulater.populateComplexEvent(clonedEvent, events.get(s));
	        	returnEventChunk.add(clonedEvent);
        }
        
        nextProcessor.process(returnEventChunk);
        streamEventChunk.clear();     
	}

	private HashMap<String, String[]> processSingleEvents(HashMap<String, String[]> events) {
		HashMap<String,String[]> events2 = new HashMap<String, String[]>();
        HashMap<String,String[]> events2process = new HashMap<String, String[]>();
        
        Set<String> events_targets = events.keySet();
        for (String s : events_targets) {
        	if((events.get(s))[EventFields.Aggregated_AGGRID.ordinal()].split(",").length>1) {
        		events2.put(s, events.get(s));
        	}else {
        		events2process.put(s, events.get(s));
        	}
        }
        
        for (String s : events2process.keySet()) {
        	
	        boolean ContainKey = events2.containsKey((String) events2process.get(s)[EventFields.Aggregated_TARGET_IP.ordinal()]);
	        if (ContainKey) {
	        	String[] aux = events2.get(events2process.get(s)[EventFields.Aggregated_TARGET_IP.ordinal()]);
	        	for (int i = 0; i < aux.length; i++) {
	        		if (i!= EventFields.Aggregated_DETECTTIME.ordinal() &&
	        				i != EventFields.Aggregated_MAQUALITY.ordinal() &&
	        				i != EventFields.Aggregated_UID.ordinal() &&
	        				i != EventFields.Aggregated_RULE.ordinal()) {    			
	        			String current_attribute = (String)events2process.get(s)[i];
            			if (current_attribute!= null) {
            				Set<String> subElements = new HashSet<String>(Arrays.asList(aux[i].split(",")));
            				subElements.addAll(Arrays.asList(current_attribute.split(",")));
            				subElements.remove("undefined");
            				subElements.remove("");
            				subElements.remove(null);
            				aux[i]=StringUtils.join(subElements,",");

            			}
	        		}else if (i == EventFields.Aggregated_MAQUALITY.ordinal()) {
	        			if (Float.parseFloat(String.valueOf(events2process.get(s)[i])) > Float.parseFloat(aux[i])) {
	        				aux[i] = String.valueOf(events2process.get(s)[i]);            				
	        			}
	        		}
	        	}
	        	events2.put(events2process.get(s)[EventFields.Aggregated_TARGET_IP.ordinal()],aux);
	        	
	        }else {
	         	events2.put(events2process.get(s)[EventFields.Aggregated_TARGET_IP.ordinal()],
	         			new String[] {(String)events2process.get(s)[EventFields.Aggregated_DETECTTIME.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_AGGRID.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_SOURCE_IP.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_SOURCE_PROTO.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_SOURCE_PORT.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_SOURCE_HOSTNAME.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_TARGET_IP.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_TARGET_PROTO.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_TARGET_PORT.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_TARGET_HOSTNAME.ordinal()],
	         					(String)events2process.get(s)[EventFields.Aggregated_CATEGORY.ordinal()],
	         					String.valueOf(events2process.get(s)[EventFields.Aggregated_MAQUALITY.ordinal()]),
	         					(String)events2process.get(s)[EventFields.Aggregated_EMAIL.ordinal()],
	         					UUID.randomUUID().toString(),"TimeWindow_Events"});
	        }
        }
		return events2;
	}
	
	private String getEmailDomain(String email) {
		if(email!=null && !email.isEmpty() && email.indexOf("@")!=-1) {
			return email.substring(email .indexOf("@") + 1);
		}else {
			return null;
		}
	}
}
