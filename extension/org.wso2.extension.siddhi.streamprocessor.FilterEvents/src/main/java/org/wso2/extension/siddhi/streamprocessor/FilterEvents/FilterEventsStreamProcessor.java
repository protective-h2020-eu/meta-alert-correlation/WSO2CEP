/*
 * Copyright (c) 2016, WSO2 Inc. (http://www.wso2.org) All Rights Reserved.
 *
 * WSO2 Inc. licenses this file to you under the Apache License,
 * Version 2.0 (the "License"); you may not use this file except
 * in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied. See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.wso2.extension.siddhi.streamprocessor.FilterEvents;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Set;

import org.wso2.siddhi.core.config.ExecutionPlanContext;
import org.wso2.siddhi.core.event.ComplexEventChunk;
import org.wso2.siddhi.core.event.stream.StreamEvent;
import org.wso2.siddhi.core.event.stream.StreamEventCloner;
import org.wso2.siddhi.core.event.stream.populater.ComplexEventPopulater;
import org.wso2.siddhi.core.executor.ExpressionExecutor;
import org.wso2.siddhi.core.executor.VariableExpressionExecutor;
import org.wso2.siddhi.core.query.processor.Processor;
import org.wso2.siddhi.core.query.processor.stream.StreamProcessor;
import org.wso2.siddhi.query.api.definition.AbstractDefinition;
import org.wso2.siddhi.query.api.definition.Attribute;

/**
 * FilterEvents.
 */
public class FilterEventsStreamProcessor extends StreamProcessor {
	
	private int paramCount = 0;
    private VariableExpressionExecutor ID; 
        	
	@Override
	public void start() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void stop() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object[] currentState() {
		return new Object[0];
	}

	@Override
	public void restoreState(Object[] arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	protected List<Attribute> init(AbstractDefinition arg0, ExpressionExecutor[] arg1, ExecutionPlanContext arg2) {
		
		paramCount = attributeExpressionLength;
		ID = (VariableExpressionExecutor) attributeExpressionExecutors[1];
				
		String[] Event_Attributes = {"Filtered_DetectTime", "Filtered_id", "Filtered_Source_IP4", "Filtered_Source_Proto", "Filtered_Source_Port", "Filtered_Source_Hostname", "Filtered_Target_IP4" , "Filtered_Target_Proto", "Filtered_Target_Port", "Filtered_Target_Hostnmae" , "Filtered_Category", "Filtered_Quality", "Filtered_Reputation" , "Filtered_Completeness", "Filtered_IpRecurrence", "Filtered_SourceRelevance", "Filtered_AttackFreshness"};
		
		ArrayList<Attribute> attributes = new ArrayList<Attribute>(paramCount);
		for (int itr = 0; itr < Event_Attributes.length; ++itr) {
	        attributes.add(new Attribute(Event_Attributes[itr],Attribute.Type.STRING));
	    }
	    return attributes;
	}

	@SuppressWarnings("deprecation")
	@Override
	protected void process(ComplexEventChunk<StreamEvent> streamEventChunk, Processor nextProcessor, StreamEventCloner streamEventCloner,
		ComplexEventPopulater complexEventPopulater) {
		
		StreamEvent streamEvent = null;
		ComplexEventChunk<StreamEvent> returnEventChunk = new ComplexEventChunk<StreamEvent>();		
		HashMap<String,String[]> events = new HashMap<String, String[]>();
        while (streamEventChunk.hasNext()) {
        	streamEvent = streamEventChunk.next();      
        	
        	
        	if (streamEvent.getBeforeWindowData()[10].toString().contains("DoS") && events.containsKey((String) ID.execute(streamEvent)+","+(String)streamEvent.getBeforeWindowData()[10])) {
            	String[] aux = events.get((String) ID.execute(streamEvent)+","+(String)streamEvent.getBeforeWindowData()[10]);
            	for (int i = 0; i < aux.length; ++i) {
            		if (i > 1 && i < 11) {    			
            			String current_attribute = (String)streamEvent.getBeforeWindowData()[i];
            			if (current_attribute!= null) {
            				for (String sub_element: current_attribute.split(",")) {
    	            			if (!aux[i].contains(sub_element) && !sub_element.equals("undefined")) {
    	            				if (aux[i].equals("undefined")) {
    	            					aux[i] = sub_element;
    	            				}else {
    	            					aux[i] = aux[i] + "," + sub_element;
    	            				}    	            				
    	            			}
            				}
            			}
            		}
            	}
            	events.put((String) ID.execute(streamEvent)+","+(String)streamEvent.getBeforeWindowData()[10],aux);            	
            }else {
            	events.put((String) ID.execute(streamEvent)+","+(String)streamEvent.getBeforeWindowData()[10], new String[] {(String)streamEvent.getBeforeWindowData()[0],(String)streamEvent.getBeforeWindowData()[1], (String)streamEvent.getBeforeWindowData()[2],(String)streamEvent.getBeforeWindowData()[3], (String)streamEvent.getBeforeWindowData()[4],(String)streamEvent.getBeforeWindowData()[5], (String)streamEvent.getBeforeWindowData()[6],(String)streamEvent.getBeforeWindowData()[7], (String)streamEvent.getBeforeWindowData()[8] , (String)streamEvent.getBeforeWindowData()[9], (String)streamEvent.getBeforeWindowData()[10], String.valueOf(streamEvent.getBeforeWindowData()[11]), String.valueOf(streamEvent.getBeforeWindowData()[12]), String.valueOf(streamEvent.getBeforeWindowData()[13]), String.valueOf(streamEvent.getBeforeWindowData()[14]), String.valueOf(streamEvent.getBeforeWindowData()[15]), String.valueOf(streamEvent.getBeforeWindowData()[16])});
            }
        }
       
        Set<String> events_targets = events.keySet();
        for (String s : events_targets) {
        	 	StreamEvent clonedEvent = streamEventCloner.copyStreamEvent(streamEvent);
	        	complexEventPopulater.populateComplexEvent(clonedEvent, events.get(s));
	        	returnEventChunk.add(clonedEvent);
        }
        
        nextProcessor.process(returnEventChunk);
        streamEventChunk.clear();     
	}
}
