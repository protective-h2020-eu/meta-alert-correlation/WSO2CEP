# Correlation module with WSO2

### Repository distribution

The technology used to perform the correlation was decided to be WSO2das, which one of their components is Complex Event Processor (CEP). At this moment, in PROTECTIVE, we are just using CEP functionalities in the correlation phase, so during the development and testing, a WSO2cep instance is being used because it is lighter and a bit faster to deploy than WSO2das (it has more functionalities such as batch processing, script execution, etc.).

WSO2cep is 100% compatible with WSO2das, so once a new functionality has been developed and tested with CEP it is copied to DAS. 


### Introduction

The aim of this document is to briefly explain the technology used to perform the correlation engine as well as expose in detail the distinct rules used to detect security patterns.
WSO2 is an open source technology provider that increases the agility of digital businesses and enterprises engaging in digital transformation. It offers an integrated enterprise platform for APIs, applications, and web services—locally and across the internet. One of the modules present in WSO2 is WSO2 Complex Event Processor (WSO2 CEP).
WSO2 Complex Event Processor (WSO2 CEP) was created as a mechanism to offer a standalone real-time analytics solution. WSO2 CEP helps identify the most meaningful events and patterns from multiple data sources, analyze their impacts, and act on them in real time. 100% open source, it allows you a set up a more agile connected business, responding to urgent business situations with both speed and precision.
The election of WSO2 over other similar solutions is due to the scalability and flexibility offered by their architecture, as it can be seen in below's image  



![WSO2 Architecture](Images/Complex_Event_Processing_An_Introduction_0.png)



WSO2 can receives data from a wide variety of data sources, and publish the results in distinct formats, such as SMS or HTTP. At the heart of WSO2 CEP is **Siddhi**. Configured using a rich, compact, easy to learn SQL-like language, WSO2 CEP is especially suited for complex queries involving time windows, as well as patterns and sequences detection. CEP queries can be changed dynamically at runtime using templates. The core streaming engine is low-latency, extremely small in size and can be deployed for edge analytics - that is, the execution of event processing flows on a device or IoT gateway. This, coupled with its ability to handle extremely large volumes of event streams, makes WSO2 CEP particularly well-suited to handle IoT scenarios.



### WSO2 CEP Architecture

The CEP engine is composed by distinct agents listed as follows:

1.	**Event Receiver**
Event receivers receive events that are coming to the CEP. WSO2 CEP supports the most common adapter implementations by default. For specific use cases, you can also plug custom adapters.

2.	**Event Streams**
Event streams contain unique sets of attributes of specific types that provide a structure based on which the events processed by the relevant event flow are selected. Event streams are stored as stream definitions in the file system via the data bridge stream definition store.

3.	**Execution Plans**
Event processor handles actual event processing. It is the core event processing unit of the CEP. It manages different execution plans and processes events based on logic, with the help of different Siddhi queries. Event Processor gets a set of event streams from the Event Stream Manager, processes them using Siddhi engine, and triggers new events on different event streams back to the Event Stream Manager.

4.	**Event Publishers**
Event publishers publish events to external systems and store data to databases for future analysis. Like the event receivers, this component also has different adapter implementations. The most common ones are available by default in the CEP. You can implement custom adapters for specific use cases.

### Flow


![WSO2 Architecture](Images/wso2_flow.PNG)  



### Implemented Rule-based execution plans

1.	**DetectSabotage**
Warn if some Sabotage action is preceded by some potential system compromise activity (Intrusion, unauthorized action, attempt of exploiting or anomaly connection) within 10 minutes

2. **Intrusion_and_malware**
Warn if an Intrusion or Attempt to a Target IP is followed by a Malware events from the same Target_IP

3.	**Multiple_Recon_Scanning_SameSource**
Warn if a Source performs 3 Scans events within a minute

4.	**Multiple_Recon_Scanning**
Warn if a target receives 3 Scans events within a minute

5.	**Exploit_and_UnauthorizedAction**
Warn whether an Attempt.Exploit or Attempt.NewSignature event is followed by some unauthorized action (Information category event) for the same target within 10 minutes

6. **UnusualUDP_Traffic**
Report Anomaly.Traffic events using UDP protocol

7.	**Exploit_and_Anomaly**
Warn if a Target generates an Anomaly category event after an attempt exploit event within 10 minutes

8. **LoginBruteforce_and_Connect**
Warn whether multiple login attempts are followed by an anomaly connection

9.	**Attacked_Target_Compromised**
Warn whether the source of an attack was previously the destination of an attack (within 10 minutes)

10. **Multiple_AnomalyConnection**
Warn if multiple anomaly connections to a same Target IP are detected

11.	**Scanning_and_Connect_Backward**
Warn if a host scan is made by an IP and then if a successful connection is established (Anomaly.Connection, Information.UnauthorizedAccess or Intrusion event category) by the same IP and then backward connection is established from connected IP to connecting IP within 10 minutes

12. **Multiple_AnomalyConnection_DistinctPort**
Warn if multiple anomaly connections to a same Target IP and distinct ports are detected

13. **Fake_DHCPServer**
Warn if an anomaly traffic event, using the UDP protocol and the port 67 or 68 is detected

14.	**Bruteforce_Login_SameTarget**
Warn if three login attempts events are registered from the same source to the same target within 10 minutes

15.	**Phising_and_MalwerInfection**
Warn if a Phising event to a Target IP is followed by a Malware event from the same Target IP (It could mean that the phising worked and the target is now infected) within 10 minutes

16.	**UnauthorizedAction_OpenBackdoor**
Warn if an unauthorized action events is followed by a vulnerable alert event. (It could means that the attacker opened a backdoor) within 10 minutes

17.	**Scanning_and_Exploit**
Warn if a Scan event is followed by an exploit attempt from same source within 10 minutes

18.	**Scanning_and_Connect**
Warn if a Scan event is followed by an anomaly connection from same source within 10 minutes

19.	**Bruteforce_login_DistintTarget**
Warn whether a Source performs three login attempts within a minute to distinct Targets

20.	**Scanning_and_Login**
Warn if a Scan event is followed by a Login attempt from same source within 10 minutes

### Implemented Temporal-based execution plans

1. **TimeWindow_Events**
Aggregate events using Source IP as unique key identifier and Target IP for the alerts not correlated by Source IP in 15 min time window.


2. **TimeWindow_MetaAlert**
Aggregates the meta-alerts with the same rule and correlated from same alerts.
