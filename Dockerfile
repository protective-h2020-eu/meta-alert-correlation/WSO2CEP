FROM registry.gitlab.com/protective-h2020-eu/meta-alert-correlation/wso2cep/clean-image:4.2.0

MAINTAINER Antonio Perez <apbautista@gmv.com>

COPY ./executionplans/* /opt/wso2/wso2cep-4.2.0/repository/deployment/server/executionplans/ 
COPY ./eventpublishers/* /opt/wso2/wso2cep-4.2.0/repository/deployment/server/eventpublishers/ 
COPY ./eventreceivers/* /opt/wso2/wso2cep-4.2.0/repository/deployment/server/eventreceivers/
COPY ./eventstreams/* /opt/wso2/wso2cep-4.2.0/repository/deployment/server/eventstreams/
COPY ./extensionJAR/* /opt/wso2/wso2cep-4.2.0/repository/components/dropins/

COPY ./perf-tuning/sysctl.conf /etc/
COPY ./perf-tuning/data-bridge-config.xml /opt/wso2/wso2cep-4.2.0/repository/conf/data-bridge/

COPY ./perf-tuning/carbon.xml /opt/wso2/wso2cep-4.2.0/repository/conf/
COPY ./perf-tuning/web.xml /opt/wso2/wso2cep-4.2.0/repository/conf/tomcat/carbon/WEB-INF/

EXPOSE 9763 9443 

ENTRYPOINT ["/opt/wso2/wso2cep-4.2.0/bin/wso2server.sh"] 

